with open("C:/Users/maras/Documents/aoc-2023/day1/day1puzzle1.txt") as f:
    sum = 0
    for line in f:
        line_nums = [x for x in line if x.isnumeric()]
        num = line_nums[0] + line_nums[-1]
        sum += int(num)
    print(sum)