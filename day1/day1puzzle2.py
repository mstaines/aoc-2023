with open("C:/Users/maras/Documents/aoc-2023/day1/day1puzzle1.txt") as f:
    sum = 0
    #Had to go to reddit, turns out sevenine should parse to 79 and not 7ine. Hence the weird workaround in the map
    num_map = {
        'one' : 'o1e',
        'two' : 't2o',
        'three' : 't3e',
        'four' : 'f4r',
        'five' : 'f5e',
        'six' : 's6x',
        'seven' : 's7n',
        'eight' : 'e8t',
        'nine' : 'n9e',
    }
    for line in f:
        line_orig = line
        for n in num_map:
            line = line.replace(n, num_map[n])
        line_nums = [x for x in line if x.isnumeric()]
        num = line_nums[0] + line_nums[-1]
        sum += int(num)
    print(sum)